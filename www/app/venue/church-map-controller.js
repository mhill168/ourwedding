(function () {
	'use strict';

	angular.module('OurWeddingApp').controller('ChurchMapController', ['$stateParams', 'feedApi', ChurchMapController]);

	function ChurchMapController($stateParams, feedApi) {

		var vm = this;

		vm.churchId = Number($stateParams.id);

		feedApi.getVenuesData().then(function(data) {

            vm.map = {
                center : {
                    latitude : data.church.latitude,
                    longitude :  data.church.longitude
                },
                zoom : 12
            };

            vm.marker = {
                latitude: data.church.latitude,
                longitude: data.church.longitude,
                idKey: data.church.id,
                title: data.church.title + "<br/>(Tap for directions)",
                showWindow: true
            };

		});


        vm.locationClicked = function (marker) {
            window.location = "geo:" + marker.latitude + "," + marker.longitude;
        };
	}

})();