/**
 * Created by mhill168 on 08/12/14.
 */

(function () {
	'use strict';

	angular.module('OurWeddingApp').controller('VenueController', ['$state','feedApi', VenueController]);

	function VenueController($state, feedApi) {

		var vm = this;

		feedApi.getVenuesData().then(function (data) {
            vm.church = data.church;
            vm.reception = data.reception;
        });

	}

})();
