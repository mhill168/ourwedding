(function () {
	'use strict';

	angular.module('OurWeddingApp').controller('StarterController', ['$stateParams', 'feedApi', '$state', StarterController]);

	function StarterController($stateParams, feedApi, $state) {

		var vm = this;

        console.log($stateParams);

		vm.starterId = Number($stateParams.id);

        feedApi.getMenuData().then(function(data) {
            vm.starter = _.chain(data.starters)
                .filter(findId)
                .map(function (item) {
                    return {
                        id: item.id,
                        name: item.name,
                        type: item.type,
                        description: item.description,
                        imgSrc: item.imgSrc
                    }
                })
                .value();
        });

        function findId(item) {
            return item.id === vm.starterId;
        }

		console.log("$stateParams", $stateParams);

	}

})();