(function () {
	'use strict';

	angular.module('OurWeddingApp').controller('MainMealController', ['$stateParams', 'feedApi', '$state', MainController]);

	function MainController($stateParams, feedApi, $state) {

		var vm = this;

        console.log($state);

		vm.mainId = Number($stateParams.id);

        feedApi.getMenuData().then(function(data) {
            vm.main = _.chain(data.mains)
                .filter(findMainId)
                .map(function (item) {
                    return {
                        id: item.id,
                        name: item.name,
                        description: item.description,
                        imgSrc: item.imgSrc
                    }
                })
                .value();
        });

        function findMainId(item) {
            return item.id === vm.mainId;
        }

		console.log("$stateParams", $stateParams);

	}

})();