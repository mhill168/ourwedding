(function () {
	'use strict';

	angular.module('OurWeddingApp').controller('EveningMealController', ['$stateParams', 'feedApi', '$state', EveningController]);

	function EveningController($stateParams, feedApi, $state) {

		var vm = this;

        console.log($state);

		vm.eveningId = Number($stateParams.id);

        feedApi.getMenuData().then(function(data) {
            vm.evening = _.chain(data.evening)
                .filter(findId)
                .map(function (item) {
                    return {
                        id: item.id,
                        name: item.name,
                        description: item.description,
                        imgSrc: item.imgSrc
                    }
                })
                .value();
        });

        function findId(item) {
            return item.id === vm.eveningId;
        }

		console.log("$stateParams", $stateParams);

	}

})();