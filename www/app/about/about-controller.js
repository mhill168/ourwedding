/**
 * Created by mhill168 on 08/12/14.
 */

(function () {

	'use strict';

	angular.module('OurWeddingApp').controller('AboutController', ['$state','feedApi', '$scope', '$ionicNavBarDelegate', '$ionicModal', AboutController]);

	function AboutController($state, feedApi,$scope, $ionicNavBarDelegate, $ionicModal) {
		
		var vm = this;

		// Disable back button on this controller
		$ionicNavBarDelegate.showBackButton(false);

		feedApi.getAboutData().then(function(data) {
			vm.bride = data.bride;
			vm.groom = data.groom;
		});


        // Create the login modal that we will use later
        $ionicModal.fromTemplateUrl('app/about/his.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.his = modal;
        });
        // Create the login modal that we will use later
        $ionicModal.fromTemplateUrl('app/about/hers.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.hers = modal;
        });

        // Triggered in the login modal to close it
        $scope.closeHisImages = function () {
            $scope.his.hide();
        };
        // Triggered in the login modal to close it
        $scope.closeHersImages = function () {
            $scope.hers.hide();
        };

        // Open the login modal
        $scope.showHisImages = function () {
            $scope.his.show();
        };
        // Open the login modal
        $scope.showHersImages = function () {
            $scope.hers.show();
        };

		// Called each time the slide changes
		vm.slideChanged = function (index) {
			$scope.slideIndex = index;
		};

	}

})();
