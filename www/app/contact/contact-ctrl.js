/**
 * Created by mhill168 on 06/01/15.
 */

(function () {

	'use strict';

	angular.module('OurWeddingApp').controller('ContactController', ["$scope", ContactController]);

	function ContactController($scope) {

		// Form data for the login modal
		$scope.userForm = {};

		// Perform the login action when the user submits the login form
		$scope.contact = function (isvalid) {

			if(isvalid) {
				console.log($scope.userForm.name);
				$scope.userForm = {};
			}

		};
	}

})();