(function () {
	'use strict';

	angular.module('OurWeddingApp').factory('feedApi', ['$http', '$q', '$ionicLoading', '$timeout', feedApi]);

    function feedApi($http, $q, $ionicLoading, $timeout) {

        var vm = this;

		function getAboutData() {

            vm.loadingIndicator = $ionicLoading.show({
                content: 'Loading Data',
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 400,
                showDelay: 10
            });

			var deferred = $q.defer();

			$http.get("app/services/json/about.json")
				.success(function (data) {
                    console.log("HTTP call went as planned");
                    $timeout(function() {
                        vm.loadingIndicator.hide();
                        deferred.resolve(data);
                    },500);
				})
				.error(function () {
					console.log("Error while making HTTP call");
					$timeout(function () {
						vm.loadingIndicator.hide();
						deferred.reject();
					}, 500)
				});

			return deferred.promise;
		}

		function getMenuData() {

            vm.loadingIndicator = $ionicLoading.show({
                content: 'Loading Data',
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 400,
                showDelay: 10
            });

			var deferred = $q.defer();

			$http.get("app/services/json/menu.json")
				.success(function (data) {
                    console.log("HTTP call went as planned");
                    $timeout(function() {
                        vm.loadingIndicator.hide();
                        deferred.resolve(data);
                    },500);
				})
				.error(function () {
					console.log("Error while making HTTP call");
					$timeout(function () {
						vm.loadingIndicator.hide();
						deferred.reject();
					}, 500)
				});

			return deferred.promise;
		}

        function getVenuesData() {

            vm.loadingIndicator = $ionicLoading.show({
                content: 'Loading Data',
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 400,
                showDelay: 0
            });

            var deferred = $q.defer();

            $http.get("app/services/json/venue.json")
                .success(function (data, status) {
                    $timeout(function() {
                        vm.loadingIndicator.hide();
                        deferred.resolve(data);
                    },500);
                })
                .error(function () {
                    console.log("Error while making HTTP call");
                    $timeout(function () {
                        vm.loadingIndicator.hide();
                        deferred.reject();
                    }, 500)
                });

            return deferred.promise;
        }

        function getOvernightData() {

            vm.loadingIndicator = $ionicLoading.show({
                content: 'Loading Data',
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 400,
                showDelay: 500
            });

            var deferred = $q.defer();

            $http.get("app/services/json/overnight.json")
                .success(function (data, status) {
                    $timeout(function() {
                        vm.loadingIndicator.hide();
                        deferred.resolve(data);
                    },500);
                })
                .error(function () {
                    console.log("Error while making HTTP call");
                    $timeout(function () {
                        vm.loadingIndicator.hide();
                        deferred.reject();
                    }, 500)
                });

            return deferred.promise;
        }

		return {
			getAboutData: getAboutData,
			getMenuData: getMenuData,
			getVenuesData: getVenuesData,
            getOvernightData: getOvernightData
		};

	}

})();

