(function () {

	'use strict';

	angular.module('OurWeddingApp').controller('LoginController', ["$scope", "$ionicModal", "$timeout", "$state", LoginController]);

	function LoginController($scope, $ionicModal, $timeout, $state) {

		ionic.Platform.ready(function () {
			// hide the status bar using the StatusBar plugin
//			StatusBar.hide();
		});

		$scope.myTitle = "Laura & Matthew";

		// Form data for the login modal
		$scope.loginData = {};

		// Create the login modal that we will use later
		$ionicModal.fromTemplateUrl('app/login/login.html', {
			scope: $scope
		}).then(function (modal) {
			$scope.modal = modal;
		});

		// Triggered in the login modal to close it
		$scope.closeLogin = function () {
			$scope.modal.hide();
		};

		// Open the login modal
		$scope.login = function () {
			$scope.modal.show();
		};

		// Perform the login action when the user submits the login form
		$scope.doLogin = function () {
			console.log('Doing login', $scope.loginData);

			// Simulate a login delay. Remove this and replace with your login
			// code if using a login system
			$timeout(function () {
				$scope.closeLogin();
			}, 500);

			// Once logged in, set the data back to empty object (this will clear the input fields)
			$scope.loginData = {};

			// Controls the splash screen on load
			console.log("Main Controller");

			setTimeout(function () {
				navigator.splashscreen.hide();
			}, 750);

			$scope.toIntro = function () {
				window.localStorage['didIntro'] = "false";
				$state.go("intro");
			}

		};
	}
})();